package com.cancun.lastresortdiscover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class LastresortDiscoverApplication {

    public static void main(String[] args) {
        SpringApplication.run(LastresortDiscoverApplication.class, args);
    }

}
